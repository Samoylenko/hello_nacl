#include <gtest/gtest.h>

int main(int argc, char* argv[])
{
    setenv("TERM", "xterm-256color", 0);
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}