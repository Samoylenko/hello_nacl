#ifndef TESTS_H_INCLUDED
#define TESTS_H_INCLUDED
#include <string>
#include <vector>
using namespace std;

bool exists_test (const string&);
vector<string>  init_push (const string&, const string&);
vector<string>  init_push (const string&, const string&, const string&);

#endif // TESTS_H_INCLUDED
