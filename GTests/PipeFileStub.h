#ifndef PIPEFILESTUB_H_INCLUDED
#define PIPEFILESTUB_H_INCLUDED
#include <string>
#include "/home/user/Desktop/untitled2/ToJson.h"
#include "/home/user/Desktop/untitled2/IPipe.h"
class PipeFileStub : public IPipeFile
{
public:
    explicit PipeFileStub(){}
    void push_in(string input)
    {

    }
    void push_out(string output)
    {

    }
    void read(shared_ptr<IOperation> oper);
    void write(ToJs obj);
    void readStub(vector<string> tmp,shared_ptr<IOperation> oper);
    vector<string> writeStub (ToJs obj);
    ~PipeFileStub(){}
};


#endif // PIPEFILESTUB_H_INCLUDED
