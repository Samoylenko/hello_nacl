#include "Tests.h"
#include "gtest/gtest.h"
#include <vector>
#include <fstream>//ifstream,ofstream
#include "json/json.h"
#include "PipeFileStub.h"
#include "/home/user/Desktop/untitled2/IOperation.h"
#include <unistd.h>
#include <string>

using namespace std;

static shared_ptr<PipeFileStub> p(new PipeFileStub);
static shared_ptr<IOperation> oper(new Operation);
ToJs obj;

static const string testLocation = "/home/user/Desktop";
static const string targetLocation = "/home/user";              //user's pathes
static const string testFile = testLocation + "/tmp.txt";
static const string targetFile = targetLocation + "/tmp.txt";   //test files pathes
static const string testFolder = testLocation + "/TestFold";
static const string targetFolder = targetLocation + "/TestFold";   //test files pathes

inline bool exists_test (const string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

vector<string>  init_push(const string& p1, const string& p2) {
    vector<string> tmp;
    tmp.push_back(p1);
    tmp.push_back(p2);
    p->readStub(tmp,oper);
    oper->execute(obj);
    return tmp;
}

vector<string>  init_push (const string& p1, const string& p2, const string& p3) {
    vector<string> tmp;
    tmp.push_back(p1);
    tmp.push_back(p2);
    tmp.push_back(p3);
    p->readStub(tmp,oper);
    oper->execute(obj);
    return tmp;
}

TEST (ShowCommand, Test_Show_Directory_Command) {
    vector<string> tmp, systemRes;
    tmp = init_push("SHOW", targetLocation);
    tmp = p->writeStub(obj);
    string systemCall = "ls " + targetLocation + " > " + testLocation + "/ls.txt";
    system(systemCall.c_str());
    ifstream kek;
    string res = testLocation + "/ls.txt";
    kek.open(res.c_str());
    string tmp1;
    while(getline(kek, tmp1))
    {
        systemRes.push_back(tmp1);
    }
    EXPECT_EQ(tmp.size(),systemRes.size());
}

TEST (ShowCommand, Test_Show_Unknown_Directory_Command) {
    vector<string> tmp;
    tmp = init_push("SHOW", "/home/so123");
    EXPECT_EQ(exists_test("/home/so123"), obj.complete);          //check if existance equals JSON throw status
}

TEST (CopyCommand, Test_Copy_Files_Command) {
    init_push("MKFILE", testFile);
    init_push("COPY", testFile, targetFile);
    string systemCall = "du -sh " + testFile + " | awk '{print $1}' > fileCopy.txt";
    string systemCall2 = "du -sh " + targetFile + " | awk '{print $1}' >> fileCopy.txt";
    system(systemCall.c_str());
    system(systemCall2.c_str());
    ifstream iff("fileCopy.txt");
    string tmp1, tmpp;
    getline(iff, tmp1);
    getline(iff, tmpp);
    EXPECT_EQ(exists_test(testFile), exists_test(targetFile)); //check if both files exists
    EXPECT_EQ(tmp1, tmpp);                                                                   //and if sizes are equals
}

TEST (CopyCommand, Test_Copy_Folder_Command) {
    init_push("DELETE", targetFolder);
    init_push("MKDIR", testFolder);
    init_push("COPY", testFolder, targetLocation);
    string systemCall = "du -sh " + testFolder + " | awk '{print $1}' > folderCopy.txt";
    string systemCall2 = "du -sh " + targetFolder + " | awk '{print $1}' >> folderCopy.txt";
    system(systemCall.c_str());
    system(systemCall2.c_str());
    ifstream iff("folderCopy.txt");
    string tmp1, tmpp;
    getline(iff, tmp1);
    getline(iff, tmpp);
    EXPECT_EQ(exists_test(testFolder), exists_test(targetFolder));       //same as files copy
    EXPECT_EQ(tmp1, tmpp);
}

TEST (CopyCommand, Test_Copy_Wrong_Target_Files_Command) {
    init_push("COPY", testFile, "/home/us/tmp.txt");
    EXPECT_NE(exists_test(testFile), exists_test("/home/us/tmp.txt"));
    EXPECT_EQ(obj.complete, false);
}

TEST (CopyCommand, Test_Copy_Wrong_Source_Files_Command) {
    init_push("COPY", "/home/ur/Desktop/tmp.txt", targetFile);
    EXPECT_NE(exists_test("/home/ur/Desktop/tmp.txt"), exists_test(targetFile));
    EXPECT_EQ(obj.complete, false);
}

TEST (CopyCommand, Test_Copy_Wrong_Source_Folders_Command) {
    init_push("COPY", "/home/ur/Desktop/TestFold", targetLocation);
    EXPECT_NE(exists_test("/home/ur/Desktop/TestFold"), exists_test(targetFolder));
    EXPECT_EQ(obj.complete, false);
}

TEST (CopyCommand, Test_Copy_Wrong_Target_Folders_Command) {
    init_push("COPY", testFolder, "/home/us/TestFold");
    EXPECT_NE(exists_test(testFolder), exists_test("/home/us/TestFold"));
    EXPECT_EQ(obj.complete, false);
}

TEST (DelCommand, Test_Del_Files_Command) {
    init_push("DELETE", testFile);
    EXPECT_EQ(exists_test(testFile), false);
    EXPECT_EQ(obj.complete, true);
}

TEST (DelCommand, Test_Del_Folder_Command) {
    init_push("MKDIR", testFolder);
    init_push("DELETE", testFolder);
    EXPECT_EQ(exists_test(testFolder), false);
    EXPECT_EQ(obj.complete, true);
}

TEST (DelCommand, Test_Del_Unavaible_Files_Command) {
    init_push("DELETE", "/home/ur/tmp.txt");
    EXPECT_EQ(obj.complete, false);
}

TEST (DelCommand, Test_Del_Unavaible_Folder_Command) {
    init_push("DELETE", "/home/ur/Tested");
    EXPECT_EQ(obj.complete, false);
}

TEST (CutCommand, Test_Cut_Files_Command) {
    init_push("MKFILE", targetFile);
    init_push("CUT", targetFile, testFile);
    EXPECT_NE(exists_test(testFile), exists_test(targetFile));
    EXPECT_EQ(obj.complete, true);
    init_push("DELETE", testFile);
}

TEST (CutCommand, Test_Cut_Folders_Command) {
    init_push("MKDIR", targetFolder);
    init_push("CUT", targetFolder, testLocation);
    EXPECT_NE(exists_test(testFolder), exists_test(targetFolder));
    EXPECT_EQ(obj.complete, true);
}

TEST (CutCommand, Test_Cut_Unavaible_File_Command) {
    init_push("CUT", "/home/ttp.txt", testFile);
    EXPECT_EQ(exists_test("/home/ttp.txt"), false);
    EXPECT_EQ(obj.complete, false);
}

TEST (CutCommand, Test_Cut_Unavaible_Folder_Command) {
    init_push("CUT", "/home/unnavaible", testFolder);
    EXPECT_EQ(exists_test("/home/unnavaible"), false);
    EXPECT_EQ(obj.complete, false);
}

TEST (CutCommand, Test_Cut_File_To_Bad_Path_Command) {
    init_push("MKFILE", testFile);
    init_push("CUT", testFile, "/null/tmp.txt");
    EXPECT_EQ(exists_test("/null/tmp.txt"), false);
    EXPECT_EQ(obj.complete, false);
    init_push("DELETE", testFile);
}

TEST (CutCommand, Test_Cut_Folder_To_Bad_Path_Command) {
    init_push("MKDIR", testFolder);
    init_push("CUT", testFolder, "/null/tmp");
    EXPECT_EQ(exists_test("/null/tmp"), false);
    EXPECT_EQ(obj.complete, false);
    init_push("DELETE", testFolder);
}

TEST (MakeFileCommand, Test_Makefile_Command) {
    init_push("MKFILE", targetFile);
    EXPECT_EQ(exists_test(targetFile), true);
    EXPECT_EQ(obj.complete, true);
    init_push("DELETE", targetFile);
}

TEST (MakeDirectoryCommand, Test_Makedirectory_Command) {
    init_push("MKFILE", targetFolder);
    EXPECT_EQ(exists_test(targetFolder), true);
    EXPECT_EQ(obj.complete, true);
}

TEST (RenameCommand, Test_Rename_Files_Command) {
    init_push("MKFILE", targetFile);
    string renamed = targetLocation + "/tmpp.txt";
    init_push("RENAME", targetFile, renamed);
    EXPECT_NE(exists_test(targetFile), exists_test(renamed));
    EXPECT_EQ(obj.complete, true);
}

