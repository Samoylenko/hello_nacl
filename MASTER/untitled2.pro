qTEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
SOURCES += main.cpp \
    forswitch.cpp \
    ICommand.cpp \
    IOperation.cpp \
    Pipe.cpp \
    ../jsoncpp-master/dist/jsoncpp.cpp
CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11
INCLUDEPATH+=/home/anger/Desktop/jsoncpp-master/dist
HEADERS += \
    ICommand.h \
    IPipe.h \
    IOperation.h \
    forswitch.h \
    Pipe.h \
    Pipe.h \
    ToJson.h \
    fromjs.h
