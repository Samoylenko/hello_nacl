#ifndef IOPERATION_H_INCLUDED
#define IOPERATION_H_INCLUDED
#include <vector>
#include <fstream>
#include <dirent.h>//opendir...
#include <sys/stat.h>//для mkdir
#include <cstring>
#include <string>
#include <json/json.h>
#include "ICommand.h"
#include <queue>
#include <memory>
using namespace std;

class IOperation{
public:
    explicit IOperation()
    {}

    virtual void addWorker(Json::Value & worker) = 0;

    virtual void execute(ToJs& obj) = 0;

    virtual ~IOperation(){};
};

class Operation : public IOperation{
protected:
    queue<shared_ptr<ICommand>> workerQ;

public:
    explicit Operation()
    {}

    void addWorker(Json::Value & worker);

    void execute(ToJs& obj)
    {
        obj = workerQ.front()->execute();
        workerQ.pop();
    }

    ~Operation()
    {}
};
#endif // IOPERATION_H_INCLUDED
