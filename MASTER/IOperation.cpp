#include "IOperation.h"
#include "ICommand.h"
#include "forswitch.h"


    void Operation::addWorker(Json::Value& worker) {//вызываем требуемый конструктор и функцию
        forswitch a;
        switch( a.parse_commd(worker.get("title","").asString()))
        {
            case DEM_SHOW:
            {
                workerQ.push(unique_ptr<ICommand>(new ShowCommand(worker.get("path","").asString())));
                break;
            }
            case DEM_COPY:
            {
                workerQ.push(unique_ptr<ICommand>(new CopyCommand(worker.get("source","").asString(),
                                        worker.get("target","").asString())));
                break;
            }
            case DEM_DEL:
            {
                workerQ.push(unique_ptr<ICommand>(new DelCommand(worker.get("path","").asString())));
                break;
            }
            case DEM_CUT:
            {
                workerQ.push(unique_ptr<ICommand>(new CutCommand(worker.get("source","").asString(),
                                       worker.get("target","").asString())));
                break;
            }
            case DEM_RENAME:
            {
                workerQ.push(unique_ptr<ICommand>(new CutCommand(worker.get("source","").asString(),
                                       worker.get("target","").asString())));
                break;
            }
            case DEM_MKDIR:
            {
                workerQ.push(unique_ptr<ICommand>(new MkdirCommand(worker.get("path","").asString())));
                break;
            }
            case DEM_SEARCH:
            {
                workerQ.push(unique_ptr<ICommand>(new SearchCommand(worker.get("source","").asString(),
                                          worker.get("target","").asString())));
                break;
            }
            case DEM_MKFILE:
            {
                workerQ.push(unique_ptr<ICommand>(new MkfileCommand(worker.get("path","").asString())));
                break;
            }
        }
    }
