#ifndef PIPE
#define PIPE
#include "IPipe.h"
#include "ToJson.h"
#include <string>
class PipeFile : public IPipeFile
{
protected:
    string in;
    string out;
public:
    PipeFile();
    void push_in(string input)
    {
        in=input;
    }
    void push_out(string output)
    {
        out=output;
    }
    virtual void read(shared_ptr<IOperation> oper);
    virtual void write(ToJs obj);
};


#endif // PIPE

