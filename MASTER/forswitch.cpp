#include "forswitch.h"
#include <cstring>
#include <vector>
forswitch::forswitch() : oper({"SHOW", "COPY", "DELETE", "CUT", "RENAME", "MKDIR", "SEARCH","MKFILE"}){};
forswitch::~forswitch(){};

int forswitch::parse_commd(std::string str)
{
    for(unsigned int i = 0; i < oper.size(); ++i)
        {
            if(oper[i] == str)
                return i+1;
        }
    return DEM_ERROR;
}
