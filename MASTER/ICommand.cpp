#include <dirent.h>
#include <unistd.h>
#include "ICommand.h"
#include "ToJson.h"
#include <sstream>
#include <iostream>
#include "Pipe.h"
using namespace std;

CopyCommand::~CopyCommand(){};

    ToJs CopyCommand::returnCopy(vector<string>& parsed_command){
        ToJs obj;
        obj.title="Copy_Command";
     struct dirent *entry;
            DIR* dir;
            dir = opendir(parsed_command[0].c_str());
            if (!dir)//на случай отсутствия доступа к папке
            {

                obj.complete=false;
                return obj;
            }
            while ( (entry = readdir(dir)) != NULL)
            {
                if(entry->d_type==4&&entry->d_name[0]!='.')//выводим папки//не выводим с точкой, ибо это скрытые файлы
                {
                   vector<string> newvector;
                   string newpath;//новая папка для поиска
                   newpath=parsed_command[0]+"/"+entry->d_name;
                   string newtocopy;
                   newtocopy=parsed_command[1]+"/"+entry->d_name;
                   mkdir(newtocopy.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                   newvector.push_back(newpath);//заменяем наш текущий путь
                   newvector.push_back(newtocopy);
                   returnCopy(newvector);//рекурсия
                }
            }
            closedir(dir);
            dir = opendir(parsed_command[0].c_str());
            while ( (entry = readdir(dir)) != NULL)
            {
                if(entry->d_type!=4&&entry->d_name[0]!='.')//теперь файлы
                {
                    string newpath;//новая папка для поиска
                    newpath=parsed_command[0]+"/"+entry->d_name;
                    string newtocopy;
                    newtocopy=parsed_command[1]+"/"+entry->d_name;
                    ifstream src(newpath.c_str(), ios::binary);//исходник
                    ofstream dst(newtocopy.c_str(), ios::binary);//копия
                    dst << src.rdbuf();
                    src.close();
                    dst.close();
                }
            }
            closedir(dir);
            obj.complete=true;
            return obj;
    }

    ToJs CopyCommand::execute() {
        DIR* dir;
        dir = opendir(source.c_str());
        ToJs obj;
        obj.title="Copy_Command";
        if(!dir){
            struct stat sb;
            if( stat(source.c_str(), &sb)==-1)
            {
                obj.complete=false;
            }
            else
            {
                obj.complete=true;
                ifstream src(source.c_str(), ios::binary);//исходник
                ofstream dst(target.c_str(), ios::binary);//копия
                dst << src.rdbuf();
                src.close();
                dst.close();
            }
            return obj;

        }
        else{
                string tmp = source;
                tmp = tmp.substr(tmp.find_last_of('/'),tmp.length());
                target+=tmp;
                mkdir(target.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                dir=opendir(target.c_str());
                if(!dir)
                {
                    obj.complete=false;
                    return obj;
                }
                vector<string> p;
                p.push_back(source);
                p.push_back(target);
                return returnCopy(p);
            }

    }

ShowCommand::~ShowCommand(){};
    ToJs ShowCommand::returnShow(vector<string>& parsed_command){
        ToJs obj;
        obj.title="Show_Command";
        obj.complete=true;
        struct dirent *entry;
        DIR* dir;
        dir = opendir(parsed_command[0].c_str());
        if(!dir){
            obj.complete=false;
            return obj;
        }
        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_name[0]!='.')//выводим папки//не выводим с точкой, ибо это скрытые файлы
            {
             //   stringstream buffer;
               // cout.rdbuf(buffer.rdbuf());
                string out;
                if(entry->d_type==4)
                    out="DIR ";
                else
                    out="FILE ";
                out+=entry->d_name;
              //  out+=" ";
              //  string command="du -sh "+parsed_command[0]+"/"+entry->d_name+" | awk '{print $1}'";
              //  system(command.c_str());
             //  out+=buffer.str();
              //  buffer.str("");
                obj.data.push_back(out);
            }
        }
        closedir(dir);
        return obj;
    }
    ToJs ShowCommand::execute() {//show /home/user/Desktop
        vector<string> p;
        p.push_back(path);
        return returnShow(p);
    }

DelCommand::~DelCommand(){};
    ToJs DelCommand::returnDel(vector<string>& parsed_command){
        ToJs obj;
        obj.title="Delete_Command";
        struct dirent *entry;
        DIR* dir;
        dir = opendir(parsed_command[0].c_str());
        if (!dir){
            obj.complete=false;
            return obj;
        }
        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_type==4&&entry->d_name[0]!='.')//выводим папки//не выводим с точкой, ибо это скрытые файлы
            {
               vector<string> newvector;
               string newpath;
               newpath=parsed_command[0]+"/"+entry->d_name;
               newvector.push_back(newpath);
               returnDel(newvector);
            }
        }
        closedir(dir);
        dir = opendir(parsed_command[0].c_str());
        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_type!=4)//теперь файлы
            {
                string newpath;//новая папка для поиска
                newpath=parsed_command[0]+"/"+entry->d_name;
                remove(newpath.c_str());
            }
        }
        closedir(dir);
        rmdir(parsed_command[0].c_str());
        obj.complete=true;
        return obj;
    }
    ToJs DelCommand::execute() {//del /home/anger/dest.txt
        DIR* dir;
        dir = opendir(path.c_str());
        ToJs obj;
        if(!dir){
            obj.title="Del_Command";
            struct stat sb;
            if( stat(path.c_str(), &sb)==-1)
            {
                obj.complete=false;
            }
            else
            {
                obj.complete=true;
                remove(path.c_str());
            }
            return obj;

        }
        else{
                vector<string> p;
                p.push_back(path);
                return returnDel(p);
            }
    }

CutCommand::~CutCommand(){};
    ToJs CutCommand::returnCut(vector<string>& parsed_command){
        ToJs obj;
        obj.title="Cut_Command";
        CopyCommand* command;
        command=new CopyCommand(parsed_command[0],parsed_command[1]);
        struct stat sb;
        if( stat(parsed_command[0].c_str(), &sb)==-1)
                    {
                        obj.complete=false;
                        return obj;
                    }
                    else
                        obj.complete=true;
        string tmp = parsed_command[0];
        tmp = tmp.substr(tmp.find_last_of('/'),tmp.length());
        parsed_command[1]+=tmp;
        mkdir(parsed_command[1].c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        command->returnCopy(parsed_command);
        delete command;
        DelCommand* command2;
        command2=new DelCommand(parsed_command[0]);
        command2->returnDel(parsed_command);
        delete command2;
        return obj;
    }
    ToJs CutCommand::execute() {//cut /home/anger/source.txt /home/dest.txt
        ToJs obj;
        DIR* dir;
        dir = opendir(source.c_str());
        if(!dir)
        {
            obj.title="Cut_Command";
            struct stat sb;
            if( stat(source.c_str(), &sb)==-1)
                        {
                            obj.complete=false;
                        }
                        else
                        {
                            obj.complete=true;
                            rename(source.c_str(),target.c_str());
                        }
            return obj;
        }
        else{
            vector<string> p;
            p.push_back(source);
            p.push_back(target);
            return returnCut(p);
        }
    }

RenameCommand::~RenameCommand(){};
    ToJs RenameCommand::returnRename(vector<string>& parsed_command){
        ToJs obj;
        obj.title="Rename_Command";
        struct stat sb;
        if( stat(parsed_command[0].c_str(), &sb)==-1)
        {
            obj.complete=false;
            return obj;
        }
        else obj.complete=true;
        rename(parsed_command[0].c_str(),parsed_command[1].c_str());
        if( stat(parsed_command[1].c_str(), &sb)==-1)
            obj.complete=false;
        else obj.complete=true;
        return obj;
    }
    ToJs RenameCommand::execute() {//rename /home/anger/source.txt /home/dest.txt
        vector<string> p;
        p.push_back(source);
        p.push_back(target);
        return returnRename(p);
    }

MkdirCommand::~MkdirCommand(){};

ToJs MkdirCommand::returnMkdir(vector<string>& parsed_command){
    mkdir(parsed_command[0].c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    ToJs obj;
    obj.title="Mkdir_Command";
    struct stat sb;
    if( stat(parsed_command[0].c_str(), &sb)==-1)
    {
        obj.complete=false;
        return obj;
    }
    else obj.complete=true;
    return obj;
}
ToJs MkdirCommand::execute() {//mkdir /home/anger/dest
    vector<string> p;
    p.push_back(path);
    return returnMkdir(p);
}

SearchCommand::~SearchCommand(){};
    ToJs SearchCommand::returnSearch(vector<string>& parsed_command){
        ToJs obj;
        obj.title="Search_Command";

        struct dirent *entry;
        DIR* dir;
        char* tmppath=new char[200];
        strcpy(tmppath,parsed_command[0].c_str());
        char* tmpwhat=new char[200];
        strcpy(tmpwhat,parsed_command[1].c_str());
        dir = opendir(parsed_command[0].c_str());
        if (!dir)//на случай отсутствия доступа к папке
        {
            obj.complete=false;
            return obj;
        }
        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_type==4&&entry->d_name[0]!='.')//выводим папки//не выводим с точкой, ибо это скрытые файлы
            {
                if(strstr(entry->d_name,parsed_command[1].c_str()))//выводим только те, которые удовлетворяют условию поиска
                {
                    string out;
               //     if(entry->d_type==4)
                        out="DIR ";
                   // else
                   //     out="FILE ";
                    out+=entry->d_name;
                    obj.data.push_back(out);
                }
               vector<string> newvector;
               string newpath;//новая папка для поиска
               newpath=parsed_command[0]+"/"+entry->d_name;
               newvector.push_back(newpath);//заменяем наш текущий путь
               newvector.push_back(parsed_command[1]);//но то, что ищем - старое
               ToJs temp;
               temp=returnSearch(newvector);//рекурсия
               for(unsigned int i=0;i<temp.data.size();i++)
                    obj.data.push_back(temp.data[i]);
            }
        }
        closedir(dir);
        dir = opendir(parsed_command[0].c_str());
        while ( (entry = readdir(dir)) != NULL)
        {
            if(entry->d_type!=4&&entry->d_name[0]!='.')//теперь файлы
            {
                if(strstr(entry->d_name,parsed_command[1].c_str()))//нужные файлы
                {
                    string out;
               //     if(entry->d_type==4)
                  //      out="DIR ";
                   // else
                    out="FILE ";
                    out+=entry->d_name;
                    obj.data.push_back(out);
                }
            }
        }
        closedir(dir);
        obj.complete=true;
        return obj;
    }
    ToJs SearchCommand::execute() {//search /home/anger what_we_search
        vector<string> p;
        p.push_back(source);
        p.push_back(target);
        return returnSearch(p);
    }
MkfileCommand::~MkfileCommand(){};
    ToJs MkfileCommand::returnMkfile(vector<string>& parsed_command){
        ToJs obj;
        obj.title="Mkfile_Command";
        ofstream of(parsed_command[0].c_str());
        of.close();
        struct stat sb;
        if( stat(parsed_command[0].c_str(), &sb)==-1)
        {
            obj.complete=false;
            return obj;
        }
        else obj.complete=true;
        return obj;
    }
    ToJs MkfileCommand::execute()
    {
        vector<string> p;
        p.push_back(path);
        return returnMkfile(p);
    }
