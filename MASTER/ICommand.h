#ifndef ICOMMAND_H_INCLUDED
#define ICOMMAND_H_INCLUDED
#include <vector>
#include <fstream>
#include <dirent.h>//opendir...
#include <sys/stat.h>//для mkdir
//#include <cstring>
#include <string>
#include "ToJson.h"
using namespace std;

class ICommand{
public:
    virtual ToJs execute() = 0;
    virtual ~ICommand(){};
};

class ShowCommand : public ICommand{        //отображение содержимого папки

    ToJs returnShow(vector<string>&);
public:
    explicit ShowCommand(string path)
        :path(path)
    {}

    ToJs execute();

    ~ShowCommand();
private:
    string path;
};

class CopyCommand : public ICommand{
public:
     ToJs returnCopy(vector<string>&);
    explicit CopyCommand(string src, string tar)
        :source(src), target(tar)
    {}

    ToJs execute();

    ~CopyCommand();

   //copy /home/anger/source.txt /home/dest.txt

private:
    string source;
    string target;
};

class DelCommand : public ICommand{
public:
    ToJs returnDel(vector<string>&);
    explicit DelCommand(string path)
        :path(path)
    {}

    ~DelCommand();


    ToJs execute();

private:
    string path;
};

class RenameCommand : public ICommand{
public:

    explicit RenameCommand(string src, string tar)
        :source(src), target(tar)
    {}

    ~RenameCommand();

    ToJs execute();

private:
    ToJs returnRename(vector<string>&);
    string source;
    string target;
};

class CutCommand : public ICommand{
    ToJs returnCut(vector<string>&);
public:


    explicit CutCommand(string src, string tar)
       :source(src), target(tar)
    {}

    ~CutCommand();

    ToJs execute();
private:
    string source;
    string target;
};

class MkdirCommand : public ICommand{
public:
    ToJs returnMkdir(vector<string>&);

    explicit MkdirCommand(string path)
        :path(path)
    {}

    ~MkdirCommand();

    ToJs execute();

private:
    string  path;
};

class SearchCommand : public ICommand{
public:
    ToJs returnSearch(vector<string>&);

    explicit SearchCommand(string src, string tar)
        :source(src), target(tar)
    {}

    ~SearchCommand();

    ToJs execute();

private:
    string source;
    string target;
};

class MkfileCommand : public ICommand{
public:
    ToJs returnMkfile(vector<string>&);

    explicit MkfileCommand(string path)
        :path(path)
    {}

    ~MkfileCommand();

    ToJs execute();

private:
    string  path;
};
#endif // ICOMMAND_H_INCLUDED
