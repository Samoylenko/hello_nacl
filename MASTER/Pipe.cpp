#include "Pipe.h"
#include "ToJson.h"
#include <iostream>
#include <fstream>
#include <json/json.h>
#include "fromJS.h"
#include <queue>
#include "IOperation.h"
//#include "jsoncpp.cpp"
using namespace std;

    PipeFile::PipeFile()
    {

    };
    void PipeFile::read(shared_ptr<IOperation> oper) //читаем и парсим команду
    {
        //string command;
        Json::Reader reader;
        Json::Value root;
        while(root.get("title","Error").asString() == "Error"){
            ifstream ins(in.c_str());
            reader.parse(ins, root);
            ins.close();
        }      
        fstream clear_file(in.c_str(), ios::out);
        clear_file.close();
        oper->addWorker(root);
    }

    void PipeFile::write(ToJs obj){
        Json::Value root;
        root["title"]=obj.title;
        root["complete"]=obj.complete;
        for(unsigned int i=0;i<obj.data.size();i++)
            root["result"][i]=obj.data[i];
        ofstream ot(out.c_str());
        ot<<root;
        ot.close();
    }
