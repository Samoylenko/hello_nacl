#ifndef FORSWICH_H_INCLUDED
#define FORSWICH_H_INCLUDED
#include <vector>
#include <fstream>
#include <dirent.h>//opendir...
#include <sys/stat.h>//для mkdir
#include <cstring>
#include <string>
enum DEM
{
    DEM_ERROR = 0,
    DEM_SHOW,
    DEM_COPY,
    DEM_DEL,
    DEM_CUT,
    DEM_RENAME,
    DEM_MKDIR,
    DEM_SEARCH,
    DEM_MKFILE
};
class forswitch{
std::vector<std::string> oper;
public:
    explicit forswitch();
    virtual ~forswitch();
    int parse_commd(std::string);
};


#endif // FORSWICH_H_INCLUDED
