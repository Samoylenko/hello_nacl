#ifndef IPIPE_INCLUDED
#define IPIPE_INCLUDED
#include <vector>
//#include <fstream>
//#include <dirent.h>//opendir...
//#include <sys/stat.h>//для mkdir
//#include <cstring>
//#include <string>
#include "ToJson.h"
#include "fromJS.h"
#include "IOperation.h"
using namespace std;


class IPipeFile //интерфейс IPipeFile с чисто виртуальным read(ifstream& in)
{
public:
    explicit IPipeFile(){};
    virtual void read(shared_ptr<IOperation> oper) = 0;//получаем поток для чтения в качестве параметра

    virtual void write(ToJs obj) =0;

    virtual void push_in(string input) =0;

    virtual void push_out(string output) =0;

    virtual ~IPipeFile() {}

};



#endif // IPIPE_INCLUDED
