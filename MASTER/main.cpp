#include <sys/stat.h>//для mkdir
#include <dirent.h>//opendir...
#include <fstream>//ifstream,ofstream
#include <cstring>//strstr...
#include "Pipe.h"
#include "ICommand.h"
#include "IOperation.h"
#include "forswitch.h"
using namespace std;

int main(){
    shared_ptr<IPipeFile> p(new PipeFile);
    shared_ptr<IOperation> oper(new Operation);
    ToJs obj;
    p->push_in("/home/anger/Desktop/example.json");
    p->push_out("/home/anger/Desktop/ls.txt");
    while(1){
          p->read(oper);
          oper->execute(obj);
          p->write(obj);
    }
}
